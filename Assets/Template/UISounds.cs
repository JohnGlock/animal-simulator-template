﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UISounds : MonoBehaviour
{

    private static UISounds Instance;

    public AudioSource audioSource;

    private Dictionary<string, AudioClip> dictionary;

    private static void LoadInstance()
    {
        GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("UISounds"));
        Instance = go.GetComponent<UISounds>();
        Instance.Init();
        DontDestroyOnLoad(go);
    }

    private void Init()
    {
        dictionary = new Dictionary<string, AudioClip>();
        dictionary.Add("attack", Resources.Load<AudioClip>("Sounds/monster-1"));
		dictionary.Add("growl", Resources.Load<AudioClip>("Sounds/growl"));
    }

    public void Play(string clipName)
    {
        audioSource.PlayOneShot(dictionary[clipName]);
    }

    private void PlayWithAudioSource(string clipName)
    {
        Instance.audioSource.clip = dictionary[clipName];
        audioSource.Play();
    }

    public static void PlayClick()
    {
        if (Sounds.SoundEnabled)
        {
            if (Instance == null)
            {
                LoadInstance();
            }
            Instance.PlayWithAudioSource("click");
        }
    }

    public static void PlaySound(string clipName)
    {
        if (Sounds.SoundEnabled)
        {
            if (Instance == null)
            {
                LoadInstance();
            }
            Instance.Play(clipName);
        }
    }

    public static void PlaySoundWithAudioSource(string clipName)
    {
        if (Sounds.SoundEnabled)
        {
            if (Instance == null)
            {
                LoadInstance();
            }
            Instance.PlayWithAudioSource(clipName);
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

}