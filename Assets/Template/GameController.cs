using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{

    [Header("Labels")]
    [Space(5)]
    public Text youWinText;
    public Text pauseText;
    public Text youLoseText;

    [Header("Buttons")]
    [Space(5)]
    public GameObject ReplayButton;
    public GameObject MenuButton;
    public GameObject PausePanel;
    public GameObject BackButton;
    public GameObject PauseButton;
    public GameObject FreeLifeButton;

    [Header("Others")]
    [Space(5)]

    ShowAd AdHelper;
    private int videoPlayed = 0;
    // Use this for initialization
    void Awake()
    {
        AdHelper = GetComponent<ShowAd>();
        Invoke("ShowNextVideo", 90);
    }


    // Update is called once per frame
    void Update() {


    }

    void ShowNextVideo()
    {
        bool show = AdHelper.PlayUnityVideoAd();
        Debug.Log("RewardedVideo is show:" + show);
        if (show)
        {
            Pause();
            videoPlayed++;
            Invoke("ShowNextVideo", 300);
        }
    }

    public void ShowRVideo()
    {
        bool show = AdHelper.PlayUnityVideoAd();
        Debug.Log("RewardedVideo is show:" + show);
        if (show)
        {
            Pause();
            //Smth;
        }
    }
    public void Finish()
    {
        youWinText.enabled = true;
        ReplayButton.SetActive(true);
        MenuButton.SetActive(true);
        Time.timeScale = 0;
    }

    public void Pause() {

        youLoseText.enabled = false;
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            PausePanel.SetActive(false);
            pauseText.enabled = false;
            ReplayButton.SetActive(false);
            MenuButton.SetActive(false);
            BackButton.SetActive(false);
            PauseButton.SetActive(true);
            FreeLifeButton.SetActive(false);
        }
        else
        {
            Time.timeScale = 0;
            PausePanel.SetActive(true);
            FreeLifeButton.SetActive(true);
            pauseText.enabled = true;
            ReplayButton.SetActive(true);
            MenuButton.SetActive(true);
            BackButton.SetActive(true);
            PauseButton.SetActive(false);
            AdHelper.ShowUnityInterstitial();
        }

    }

    public void Lose()
    {
        pauseText.enabled = false;
        PausePanel.SetActive(true);
        youLoseText.enabled = true;
        ReplayButton.SetActive(true);
        MenuButton.SetActive(true);
        FreeLifeButton.SetActive(true);
        Time.timeScale = 0;

        AdHelper.ShowUnityInterstitial();
    }

    public void AddLife()
    {
        bool show = AdHelper.PlayUnityVideoAd();
        if (show)
        {
            PausePanel.SetActive(false);
            PauseButton.SetActive(true);
            Time.timeScale = 1;
            AnimalSim.playerController.NewLife();
        }
    }

	public void Menu()
    {
    
        Time.timeScale = 1;
        Application.LoadLevel("Menu");
		AdHelper.ShowUnityInterstitial ();
	}

	public void Replay()
    {
		Time.timeScale = 1;
		Application.LoadLevel("Game");

	}
}