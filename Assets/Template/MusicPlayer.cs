﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	private static MusicPlayer instance;
	public static MusicPlayer Instance{
		get{
			if(instance == null){
				GameObject go = (GameObject)Instantiate(Resources.Load("MusicPlayer"));
				instance = go.GetComponent<MusicPlayer>();
			}
			return instance;
		}
	}

	public AudioSource audioSource;

	private float volume = 1;
	[Range(0,1)]public float maxVolume = 1;
	[Range(0,1)]public float minVolume = .2f;

	private bool volumeChanging;

	private bool isOn;

	void Avake(){
		if(instance == null)
			instance = this;
		//		volume = maxVolume;
	}

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
	}

	// Update is called once per frame
	void Update () {
		if (volumeChanging) {
			if(isOn){
				volume += Time.deltaTime * .5f;
				if(volume > maxVolume){
					volume = maxVolume;
					volumeChanging = false;
				}
				audioSource.volume = volume;
			}
			else{
				volume -= Time.deltaTime * .5f;
				if(volume < minVolume){
					volume = minVolume;
					volumeChanging = false;
				}
				audioSource.volume = volume;
			}
		}
	}

	public void MaxVolume(){
		isOn = true;
		volumeChanging = true;
	}

	public void MinVolume(){
		isOn = false;
		volumeChanging = true;
	}

	public void Enabled(bool value){
		audioSource.mute = !value;
		if (value) 
			audioSource.Play ();
		else
			audioSource.Stop ();
	}

	public void Pause(){
		audioSource.Pause ();
	}

	public void Play(){
		if (Sounds.MusicEnabled) {
			audioSource.Play();
		}
	}
}
