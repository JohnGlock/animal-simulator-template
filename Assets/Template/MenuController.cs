﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public GameObject sceneLoadingPanel;
    public GameObject menuPanel;
    public Slider sliderSceneLoading;
    public Text labelSceneLoading;
    public static string AccountURI = "https://play.google.com/store/apps/developer?id=Watermelon+bone";
    public static string AppURI = "https://play.google.com/store/apps/details?id=com.watermelonbone.GalaxyBallBalance";


	void Start()
	{
		MusicPlayer.Instance.Play ();
	}

    public void MoreGames()
    {
        AppMetrica.Instance.ReportEvent("User click More Games");
        Application.OpenURL(AccountURI);
    }
    public void Rate()
    {
        AppMetrica.Instance.ReportEvent("User click Rate");
        Application.OpenURL(AppURI);
    }
    public void Game()
    {
        menuPanel.SetActive(false);
        sceneLoadingPanel.SetActive(true);
        StartCoroutine(LoadALevel("game"));
    }
    void Update()
    {
        if (async != null)
        {
            int progress = (int)(async.progress * 100);
            labelSceneLoading.text = progress.ToString() + " %";
            sliderSceneLoading.value = async.progress;
            if (progress > 90) { }
               
        }

    }
    #region LoadingLevel

    private AsyncOperation async = null;

    private IEnumerator LoadALevel(string levelName)
    {
        async = SceneManager.LoadSceneAsync(levelName);
        yield return async;
    }

    #endregion
}
