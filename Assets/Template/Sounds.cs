﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {

	private static Sounds Instance;

	private bool soundEnabled;
	private bool musicEnabled;

	public Sounds(){
		soundEnabled = PlayerPrefs.GetInt("SoundEnabled", 1) > 0;
		musicEnabled = PlayerPrefs.GetInt("MusicEnabled", 1) > 0;
	}

	public static bool SoundEnabled{
		get{
			if(Instance == null) Instance = new Sounds();
			return Instance.soundEnabled;
		}
		set{
			if(Instance == null) Instance = new Sounds();
			PlayerPrefs.SetInt("SoundEnabled", value ? 1 : -1);
			Instance.soundEnabled = value;
		}
	}

	public static bool MusicEnabled{
		get{
			if(Instance == null) Instance = new Sounds();
			return Instance.musicEnabled;
		}
		set{
			if(Instance == null) Instance = new Sounds();
			PlayerPrefs.SetInt("MusicEnabled", value ? 1 : -1);
			Instance.musicEnabled = value;
		}
	}
}
