﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Advertisements;

public class ShowAd : MonoBehaviour
{
    public void ShowUnityInterstitial()
    {

        if (Advertisement.IsReady("defaultZone"))
        {
            Advertisement.Show("defaultZone");
        }

    }

    //Show Rewarded Video
	public bool PlayUnityVideoAd()
	{
		if (Advertisement.IsReady("rewardedVideo"))
        {
            bool resultBool = true;
			Advertisement.Show("rewardedVideo", new ShowOptions
            {              
                resultCallback = result =>
                {
                    switch (result)
                    {
                        case (ShowResult.Finished):
                            resultBool = true;
                            break;
                        case (ShowResult.Failed):
                            resultBool = false;
                            break;
                        case (ShowResult.Skipped):
                            resultBool = false;
                            break;
                    }
                }
            });
            return resultBool;
        }
        return false;

	}
	
}
