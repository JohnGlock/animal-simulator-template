﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using UnityEngine.UI;


public class LevelManager : MonoBehaviour {

    [Header("UI")]
    [Space(5)]
    public GameObject StatsMenu;
    public Text TitleTxt;
    public GameObject Plus;

    [Space(5)]
    public Text UserLevelTxt;
    public Text UserExpTxt;
    public Text UserHealthTxt;
    public Text UserStaminaTxt;

    [Space(5)]

    public Text PowerLevelTxt;
    public Text HealthLevelTxt;
    public Text DamageLevelTxt;
    [Space(5)]

    public Text AvatarUserLevel;

    public int levelId;
    public int powerLevel;
    public int healthLevel;
    public int damageLevel;

    public int skillPoints=0;
   
    // Use this for initialization
    void Start()
    {
        
        levelId = 1;
        UpdateLevelUI();
    }

    void UpdateLevelUI()
    {
        AvatarUserLevel.text = levelId.ToString();
        UserLevelTxt.text = "Level: " + levelId.ToString();
        string color = (skillPoints == 0) ? "red" : "green";
        TitleTxt.text = "Available <color="+color+">" + skillPoints + "</color> points";
        UserHealthTxt.text =(int)AnimalSim.playerLiving.HP + "/" + AnimalSim.playerLiving.MaxHP;
        UserStaminaTxt.text= (int)AnimalSim.playerLiving.Stamina + "/" + AnimalSim.playerLiving.MaxStamina;
    }

    public void AddPoints(string attribute)
    {
        if (skillPoints == 0)
        {
            return;
        }
        skillPoints--;
        switch (attribute)
        {
            case "power":
                powerLevel++;
                PowerLevelTxt.text = powerLevel.ToString();
                AnimalSim.playerLiving.MaxStamina += 5;
                break;
            case "health":
                healthLevel++;
                HealthLevelTxt.text = healthLevel.ToString();
                AnimalSim.playerLiving.MaxHP += 5;
                break;
            case "damage":
                damageLevel++;
                DamageLevelTxt.text = damageLevel.ToString();
                AnimalSim.playerController.damageBoost++;
                break;
            default:
                break;
        }

        UpdatePlayerStats();
        UpdateLevelUI();
    }

    private void UpdatePlayerStats()
    {
        AnimalSim.playerLiving.needUpdateUI();
    }

    public void LevelUp()
    {
        levelId++;
        skillPoints = 5;
        UpdateLevelUI();
    }

    public void ShowStatsMenu(bool show)
    {
        StatsMenu.SetActive(show);
        Time.timeScale = (show) ? 0 : 1;
		UpdateLevelUI();
    }
    public void WatchRewardedVideo()
    {
        bool finished_ads = AnimalSim.showAd.PlayUnityVideoAd();
        if (finished_ads)
        {
            skillPoints *= 2;
            UpdateLevelUI();
        }
    }
	// Update is called once per frame
	void Update ()
    {
        Plus.SetActive(skillPoints!=0);
	}


}
