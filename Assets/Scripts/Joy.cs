using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class Joy : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
 public Canvas canvas;
 public Camera canvasCam;
 public GameObject Background;
 public GameObject Stick;

 public string horizontalAxisName = "Horizontal";
 public string verticalAxisName = "Vertical";
 public int MovementRange = 100;
 public bool IsJoystickFixed;
 public bool IsReturnMove;
 public bool IsFade;
 
 Vector3 m_StartPosStick;
 Vector3 m_StartPosBackStick;
 CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis;
 CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis;
 RectTransform bgRect;
 RectTransform stickRect;
 Image bgImage;
 Image stickImage;

 float k;

 void UpdateVirtualAxes(Vector3 value)
 {
  var delta = m_StartPosStick - value;
  delta.y = -delta.y;
  delta /= MovementRange;

  m_HorizontalVirtualAxis.Update(-delta.x);
  m_VerticalVirtualAxis.Update(delta.y);
 }

 public void OnDrag(PointerEventData eventData)
 {
  Vector3 newPos = Vector3.zero;

  int delta = (int)(eventData.position.x/* * k*/ - m_StartPosBackStick.x);
  delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
  newPos.x = delta;

  delta = (int)(eventData.position.y/* * k*/ - m_StartPosBackStick.y);
  delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
  newPos.y = delta;

  var v3 = new Vector3(m_StartPosStick.x + newPos.x, m_StartPosStick.y + newPos.y, m_StartPosStick.z + newPos.z);
        v3 *= Time.timeScale;
  stickRect.localPosition = Vector3.ClampMagnitude(v3, MovementRange);
//  stickRect.localPosition = v3;
  UpdateVirtualAxes(v3);
 }

 public void OnPointerDown(PointerEventData eventData)
 {
  if (!IsJoystickFixed)
  {
        bgRect.position = eventData.position;
  }
        m_StartPosBackStick = bgRect.position;

        if (IsFade)
  {
   bgImage.CrossFadeAlpha(1f, .1f, true);
   stickImage.CrossFadeAlpha(1f, .1f, true);
  }
 }

 public void OnPointerUp(PointerEventData eventData)
 {
  stickRect.localPosition = Vector3.zero;
  if (!IsReturnMove)
  {
   UpdateVirtualAxes(stickRect.localPosition);
  }
  if (IsFade)
  {
   bgImage.CrossFadeAlpha(0f, .5f, true);
   stickImage.CrossFadeAlpha(0f, .5f, true);
  }
 }

 void Start()
 {
  bgRect = Background.GetComponent<RectTransform>();
  stickRect = Stick.GetComponent<RectTransform>();
  bgImage = Background.GetComponent<Image>();
  stickImage = Stick.GetComponent<Image>();

  m_StartPosStick = Vector3.zero;
  m_StartPosBackStick = bgRect.anchoredPosition3D;

  if (canvasCam == null)
   k = 1f;
  else
   k = canvas.GetComponent<RectTransform>().rect.height / canvasCam.pixelHeight;
 }

 void OnEnable()
 {
  CreateVirtualAxes(); 
 }

 void OnDisable()
 {
  m_HorizontalVirtualAxis.Remove();    
  m_VerticalVirtualAxis.Remove();
 }

 void CreateVirtualAxes()
 {
  m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);

  CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);

  m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
  CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
  if (canvasCam == null)
   k = 1f;
  else
   k = canvas.GetComponent<RectTransform>().rect.height / canvasCam.pixelHeight;
 }

 public void Reset()
 {
  bgRect.anchoredPosition3D = m_StartPosBackStick;
  m_HorizontalVirtualAxis.Update(0f);
  m_VerticalVirtualAxis.Update(0f);
 }


 void Update()
 {



#if UNITY_EDITOR
        if (horizontalAxisName != "Horizontal") { return; }

  if (Input.GetKey(KeyCode.W))
  {
   m_VerticalVirtualAxis.Update(1f);
  }
  if (Input.GetKey(KeyCode.S))
  {
   m_VerticalVirtualAxis.Update(-1f);
  }
  if (Input.GetKey(KeyCode.A))
  {
   m_HorizontalVirtualAxis.Update(-1f);
  }
  if (Input.GetKey(KeyCode.D))
  {
   m_HorizontalVirtualAxis.Update(1f);
  }
  if (Input.GetKeyUp(KeyCode.W)|| Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
  {
   m_VerticalVirtualAxis.Update(0f);
  }
  if (Input.GetKeyUp(KeyCode.W)|| Input.GetKeyUp(KeyCode.S))
  {
   m_VerticalVirtualAxis.Update(0f);
  }
  if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
  {
   m_HorizontalVirtualAxis.Update(0f);
  }
#endif
    }

}