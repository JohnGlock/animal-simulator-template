﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour, HitableObject {

	public string Name;
	public enum AnimalAIState{
		None,
		Wait,
		Walk,
		Chase,
		Attack,
		Escape,
		Eating,
		Death
	};
	public AnimalAIState state;

	public bool aggressive;
	public bool defend;
	public bool afraid;

	public bool IsDead{
		get{return (state == AnimalAIState.Death);}
	}

	public UnityEngine.AI.NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
	public ThirdPersonCharacter character { get; private set; } // the character we are controlling

	Animator m_Animator;


	public float maxHealth;
	private float _health;
	public float Health{
		get{
			return _health;
		}
		set{
			if(value <= 0){
				Death();
			}
			_health = Mathf.Clamp(value, 0, maxHealth);
		}
	}


	public float aggressionDistance;
	public float leaveDistance;
	private float attackDistanceMin;
	private float attackDistanceMax;

	public float walkRadius;
	private Vector3 startPos;
	private Vector3 walkTargetPos;

	private float attackDelay = -1;
	private bool isHitting = false;

	private Transform target;

	private float waitWalkTimer;

	public float damage;

	public int meatCount = 1;
	public int containerLayer;
	public string containerName;

	public bool isHidded { get; private set;}
	public SkinnedMeshRenderer skin;


	public bool needHide;
	public GameObject DeadBody;

	public float eatingTime=5;
	public float curEatingTime;

	public float maxHeal=30;
	private float estimateHeal=0;
	// Use this for initialization
	void Start () {
		
		curEatingTime = eatingTime;
		estimateHeal = maxHeal;

		DeadBody.SetActive (false);
		agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
		character = GetComponent<ThirdPersonCharacter>();

		attackDistanceMin = agent.stoppingDistance * 1.4f;
		attackDistanceMax = agent.stoppingDistance * 1.8f;


		agent.updateRotation = false;
		agent.updatePosition = true;

		m_Animator = GetComponent<Animator>();

		_health = maxHealth;

		startPos = transform.position;

		Wait();
		if (needHide) {
			needHide = false;
			Hide();
		}
	}

	void Update(){

		if(attackDelay >= 0){
			attackDelay -= Time.deltaTime;
		}

		if (transform.position.y < -5000)
			Destroy (gameObject);
	}

	// Update is called once per frame
	void FixedUpdate () {

		if(isHidded)return;
		float playerDist = (AnimalSim.playerController != null) ? Vector3.Distance(AnimalSim.playerController.transform.position, transform.position) : -100;
		float walkDist = 0;

		switch(state){
		case AnimalAIState.None:
			character.Move(Vector3.zero, false, false);

			break;

		case AnimalAIState.Wait:
			character.Move(Vector3.zero, false, false);
			if(aggressive){
				if(CheckPlayerForAttack(playerDist)){
					Chase(AnimalSim.playerController.transform);
				}
			}
			else if(afraid){
				if(CheckPlayerForAttack(playerDist)){
					Escape();
				}
			}
			waitWalkTimer -= Time.fixedDeltaTime;
			if(waitWalkTimer < 0){
				float walkAngle = Random.Range(0f, Mathf.PI * 2);
				walkDist = Random.Range(0f, walkRadius);
				walkTargetPos = startPos + new Vector3(walkDist * Mathf.Sin(walkAngle), 0, walkDist * Mathf.Cos(walkAngle));
				Walk();
			}
			break;

		case AnimalAIState.Walk:
			try
			{
				agent.SetDestination(walkTargetPos);
			}
			catch (System.Exception ex)
			{
				Debug.Log("Error:" + ex.Message, gameObject);
			}
			character.Move(agent.desiredVelocity, false, false);
			if(aggressive){
				if(CheckPlayerForAttack(playerDist)){
					Chase(AnimalSim.playerController.transform);
				}
			}
			else if(afraid){
				if(CheckPlayerForAttack(playerDist)){
					Escape();
				}
			}
			waitWalkTimer -= Time.fixedDeltaTime;
			walkDist = Mathf.Sqrt((transform.position.x - walkTargetPos.x) * (transform.position.x - walkTargetPos.x) +
				(transform.position.z - walkTargetPos.z) * (transform.position.z - walkTargetPos.z));

			if(waitWalkTimer < 0 && walkDist < attackDistanceMin){
				Wait();
			}
			break;

		case AnimalAIState.Chase:
			if(target != null){
				float dist = Vector3.Distance(transform.position, target.position);
				if(dist < attackDistanceMin){
					Attack(target);
				}
				else if(dist > leaveDistance){
					Walk();
				}
				agent.SetDestination(AnimalSim.playerController.transform.position);
				character.Move(agent.desiredVelocity, false, false);
			}
			else{
				Wait();
			}
			break;

		case AnimalAIState.Attack:
			character.Move(Vector3.zero, false, false);
			if(target != null){
				transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
			}
			if(!isHitting){
				if(target != null){
					float dist = Vector3.Distance(transform.position, target.position);
					if(dist > attackDistanceMax){
						Chase(target);
					}
					else{
						if(attackDelay < 0){
							HitableObject ho = target.GetComponent<HitableObject>();
							if(ho != null)
								TryHit();
							else
								Wait();
						}
					}
					character.Move(Vector3.zero, false, false);
				}
				else{
					Wait();
				}
			}

			break;

		case AnimalAIState.Escape:
			if(AnimalSim.playerController != null){
				agent.SetDestination(walkTargetPos);
				character.Move(agent.desiredVelocity, false, false);
				waitWalkTimer -= Time.fixedDeltaTime;
				walkDist = (transform.position.x - walkTargetPos.x) * (transform.position.x - walkTargetPos.x) +
					(transform.position.z - walkTargetPos.z) * (transform.position.z - walkTargetPos.z);
				if(waitWalkTimer < 0 || walkDist < attackDistanceMin){
					if(playerDist > leaveDistance){
						Wait();
					}
					else{
						Escape();
					}
				}
			}
			else{
				Walk();
			}
			break;

		case AnimalAIState.Eating:

			break;

		case AnimalAIState.Death:
			if(AnimalSim.playerController != null){

			}
			else{
				Destroy(gameObject);
			}
			break;
		}


	}

	public void Hide(){
		isHidded = true;
		m_Animator.enabled = false;
		skin.enabled = false;
		if (agent != null)
			agent.enabled = false;
		//m_Rigidbody.isKinematic = true;
	}

	public void Unhide(){
		isHidded = false;
		m_Animator.enabled = true;
		skin.enabled = true;
		if(agent != null)
			agent.enabled = true;
		//m_Rigidbody.isKinematic = false;
	}


	private bool CheckPlayerForAttack(float dist){
		return (dist > 0 ? ( dist < aggressionDistance ? true : false ): false);
	}

	public void GetHit(float _damage){
		if (state != AnimalAIState.Death) {

			if(AnimalSim.playerController != null){

				float dist = Vector3.Distance(transform.position, AnimalSim.playerController.transform.position);

				if((dist < leaveDistance) && (defend || aggressive)){
					Chase(AnimalSim.playerController.transform);
				}
				else{
					Escape();
				}
			}
			Health -= _damage;
		}
	}

	private void TryHit(){
		isHitting = true;
		m_Animator.SetTrigger ("Attack");
		Debug.Log ("Start Attack");
        AnimalSim.playerLiving.GetHit(damage);

	}

	public void HitTarget(){
		if (target != null) {
			HitableObject ho = target.GetComponent<HitableObject>();
			if(ho != null){
				ho.GetHit(damage * Random.Range(0.75f, 1.25f));
			}
		}
	}

	public void EndHitting(){
		isHitting = false;
//		m_Animator.ResetTrigger("Attack");
		attackDelay = Random.Range (1.5f, 3.5f);
	}

	public void Wait(){
		state = AnimalAIState.Wait;
		waitWalkTimer = Random.Range (2f, 8f);
	}

	public void Walk(){
		state = AnimalAIState.Walk;
		waitWalkTimer = Random.Range (2f, 8f);
		//agent.Stop();
	}

	public void Chase(Transform _target){
		target = _target;
		state = AnimalAIState.Chase;
	}

	public void Attack(Transform _target){
		target = _target;
		state = AnimalAIState.Attack;
	}

	public void Escape(){
		waitWalkTimer = 2;
		Vector3 escapeDir = (transform.position - AnimalSim.playerController.transform.position).normalized;
		Vector3 sideDir = Vector3.Cross (Vector3.up, escapeDir).normalized;
		walkTargetPos = transform.position + 5 * escapeDir + Random.Range(-5f, 5f) * sideDir;
		state = AnimalAIState.Escape;
	}

	private void Death(){
		DeadBody.SetActive (true);
		m_Animator.SetTrigger ("Die");
		character.Kill();
		character = null;
		state = AnimalAIState.Death;
        AnimalSim.questController.AnimalWasKilled(Name);
		//Удаление с карты иконки животного при смерти
		Destroy (GetComponent<makeRadarObject> ());
	}



}
