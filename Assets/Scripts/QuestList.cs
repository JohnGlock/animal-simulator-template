﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

[XmlRoot("QuestsList")]
public class QuestList 
{
	[XmlElement("Version")]
	public double Version;

	[XmlArray("Quests"),XmlArrayItem("Quest")]
	public List<Quest> Quests;

	[XmlIgnore]
	private static string FileName {
		get {
			string rootFolder;
			#if UNITY_EDITOR
			rootFolder = Application.dataPath;
			#elif UNITY_ANDROID || UNITY_IOS
			rootFolder = Application.persistentDataPath;
			#endif
			string _QuestFileName=rootFolder + "/" + "XML" + "/QuestsLibrary.xml";
			return _QuestFileName;
		}
	}

	#region Load / Save & Serialization

	public void Save()
	{
		Save (FileName);
	}

	public void Save(string path)
	{
		var serializer = new XmlSerializer(typeof(QuestList));
		using(var stream = new FileStream(path, FileMode.Create))
		{
			serializer.Serialize(stream, this);
		}
	}

	public static QuestList Load (string path="")
	{
		if (path == "") {
			path = FileName;
		}
		Debug.Log (path);
		var serializer = new XmlSerializer(typeof(QuestList));
		using(var stream = new FileStream(path, FileMode.Open))
		{
			return serializer.Deserialize(stream) as QuestList;
		}
	}

	//Loads the xml directly from the given string. Useful in combination with www.text.
	public static QuestList LoadFromText(string text) 
	{
		var serializer = new XmlSerializer(typeof(QuestList));
		return serializer.Deserialize(new StringReader(text)) as QuestList;
	}

	#endregion

}
