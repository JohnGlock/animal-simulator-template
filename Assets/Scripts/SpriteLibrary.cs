﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Res=UnityEngine.Resources;

public class SpriteLibrary : MonoBehaviour {

	private static string path="animalsAvatar/";
    private static SpriteLibrary instance;
    public Dictionary<string, Sprite> Sprites;

    public static SpriteLibrary Instance()
    {
        instance = FindObjectOfType<SpriteLibrary>();

        if (instance == null)
        {
            GameObject go = new GameObject();
            instance= go.AddComponent<SpriteLibrary>();
            instance.Init();
            go.name = "SpriteLibrary";            
        }
        return instance;
    }

    private void Init()
    {
        Sprites = new Dictionary<string, Sprite>();

		foreach (string animalName in AnimalSim.Animals) {
			Sprites.Add(animalName.ToLower()+"_green",Res.Load<Sprite>(path+animalName.ToLower()+"_green"));		
			Sprites.Add(animalName.ToLower()+"_red",Res.Load<Sprite>(path+animalName.ToLower()+"_red"));	
		}

    }

	/// <summary>
	/// Send name in Lower case string
	/// </summary>
	/// <returns>The sprite by name.</returns>
	/// <param name="Name">Name.</param>
    public Sprite GetSpriteByName(string Name)
    {
        if (Sprites.ContainsKey(Name))
        {            
            return Sprites[Name];
        }
        return null;
    }

}
