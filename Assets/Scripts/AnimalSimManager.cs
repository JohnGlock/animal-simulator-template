﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSimManager : MonoBehaviour {


	void Awake()
	{
		AnimalSim.questController = (QuestController)GameObject.FindObjectOfType(typeof(QuestController));
		AnimalSim.playerController = (AnimalController)GameObject.FindObjectOfType(typeof(AnimalController));
		AnimalSim.playerLiving = (PlayerLiving)GameObject.FindObjectOfType(typeof(PlayerLiving));
		AnimalSim.animalPopulation = (AnimalPopulation)GameObject.FindObjectOfType(typeof(AnimalPopulation));
        AnimalSim.levelManager = (LevelManager)GameObject.FindObjectOfType(typeof(LevelManager));
        AnimalSim.gameController = (GameController)GameObject.FindObjectOfType(typeof(GameController));
        AnimalSim.showAd = (ShowAd)GameObject.FindObjectOfType(typeof(ShowAd));
		AnimalSim.rayCaster = (Raycaster)GameObject.FindObjectOfType (typeof(Raycaster));

        AnimalSim.gameStat=GameStatistics.Instance ();
        Debug.Log(AnimalSim.gameStat.levelNumber);
	}

}

public static class  AnimalSim
{
	public static QuestController questController;
	public static AnimalController playerController;
	public static PlayerLiving playerLiving;
	public static AnimalPopulation animalPopulation;
	public static GameStatistics gameStat;
    public static LevelManager levelManager;
    public static GameController gameController;
    public static ShowAd showAd;
	public static Raycaster rayCaster;

    public static string[] Animals=new string[]
	{
		"Gazelle",
		"Warthog",
		"Zebra",
		"Hyena",
		"Buffalo",
		"Rhino",
		"Elephant"
	};
}