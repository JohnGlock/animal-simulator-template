﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestItemUI : MonoBehaviour {

	[HideInInspector]
	public QuestItem item;

	public Text text;
	public Image avatar;

	public void SetItem(QuestItem item)
	{
		this.item = item;
		avatar.overrideSprite = SpriteLibrary.Instance ().GetSpriteByName (item.Name.ToLower()+"_green");
		text.text = item.CurrentCount + "/" + item.TotalCount;
	}

}
