﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class AnimalController : MonoBehaviour {


	public GameObject Head;
	public bool canRun;


	private float moveSpeed = 15; // move speed
	private float turnSpeed = 90; // turning speed (degrees/second)
	private float lerpSpeed = 5; // smoothing speed
	private float gravity = 20; // gravity acceleration
	private bool isGrounded;
	private float deltaGround = 1.0f; // character is grounded up to this distance
	private float jumpSpeed = 10; // vertical jump initial speed
	private float jumpRange = 10; // range to detect target wall
	private Vector3 surfaceNormal; // current surface normal
	private Vector3 myNormal; // character normal
	private float distGround; // distance from character position to ground
	private bool jumping = false; // flag &quot;I'm jumping to wall&quot;
	private float vertSpeed = 0; // vertical jump current speed
	private bool roarning=false;
	private Transform myTransform;
	public CapsuleCollider capsuleCollider; // drag BoxCollider ref in editor
	private Rigidbody rigidbody;
	private Animator animator;

    public int damageBoost=1;
	Raycaster raycaster;
	private void Start(){
		
		animator = GetComponent<Animator> ();
		rigidbody = GetComponent<Rigidbody> ();
		myNormal = transform.up; // normal starts as character up direction
		myTransform = transform;
		rigidbody.freezeRotation = true; // disable physics rotation
		// distance from transform.position to ground
		canRun=true;
		raycaster = GetComponentInChildren<Raycaster> ();

	}

	private void FixedUpdate(){
		// apply constant weight force according to character normal:
		rigidbody.AddForce(-gravity*rigidbody.mass*myNormal);
	}



	private void Update(){

        if (AnimalSim.playerLiving.isDead)
        {
            return;
        }

		canRun = !roarning;
        

        if (!canRun) {
			animator.SetFloat("Speed",0);
			return;
		}

		// movement code - turn left/right with Horizontal axis:
		myTransform.Rotate(0, CrossPlatformInputManager.GetAxis("Horizontal")*turnSpeed*Time.deltaTime, 0);
        

        CheckGroundStatus ();

		myNormal = Vector3.Lerp(myNormal, surfaceNormal, lerpSpeed*Time.deltaTime);
		// find forward direction with new myNormal:
		Vector3 myForward = Vector3.Cross(myTransform.right, myNormal);
		// align character to the new myNormal while keeping the forward direction:
		Quaternion targetRot = Quaternion.LookRotation(myForward, myNormal);
		myTransform.rotation = Quaternion.Lerp(myTransform.rotation, targetRot, lerpSpeed*Time.deltaTime);

		if (CrossPlatformInputManager.GetAxis ("Vertical") >= 0) {
			AnimalSim.playerLiving.UpdateStamina (CrossPlatformInputManager.GetAxis("Vertical"));
		}

        float speed = CrossPlatformInputManager.GetAxis("Vertical");
        // move the character forth/back with Vertical axis:
        animator.SetFloat("Speed", speed);

		moveSpeed = moveSpeed * ((canRun) ? 1f : 0.5f);

		if (CrossPlatformInputManager.GetAxis ("Vertical") > 0) {
			myTransform.Translate(0, 0, CrossPlatformInputManager.GetAxis("Vertical")*moveSpeed*Time.deltaTime);
		} else {
			//Slow Back movement
			myTransform.Translate(0, 0, CrossPlatformInputManager.GetAxis("Vertical")*moveSpeed/4*Time.deltaTime);
		}


        

		if (CrossPlatformInputManager.GetAxis ("Vertical") <= 0.5f) {
			animator.SetFloat ("Turn", CrossPlatformInputManager.GetAxis ("Horizontal"));
		} else {
			animator.SetFloat ("Turn", 0);
		}

		animator.SetBool ("Idle", CrossPlatformInputManager.GetAxis ("Vertical") == 0);

        if (CrossPlatformInputManager.GetAxis("Vertical") != 0)
        {
			animator.SetBool("Eating", false); 
			raycaster.isEating = false;            
        }

		if (isGrounded && CrossPlatformInputManager.GetAxis("Vertical")== 1.0)
		{
			if (CrossPlatformInputManager.GetButtonDown("Jump")){ // jump pressed:				
				rigidbody.velocity = new Vector3(rigidbody.velocity.x, 8, rigidbody.velocity.z);
				isGrounded = false;
				animator.applyRootMotion = false;

			}
		}
		else
		{
			HandleAirborneMovement();
		}

		animator.SetBool ("Jump",!isGrounded && Input.GetAxis("Vertical")> 0.7);


		if (CrossPlatformInputManager.GetButtonDown ("Fire2")) {	
			StartCoroutine ("Roar");
		}

		animator.SetBool("Roar", roarning);  
           
    }

	IEnumerator  Roar()
	{
		roarning = true;		  
		UISounds.PlaySound ("growl");
		yield return new WaitForSeconds (1.5f);		 
		roarning = false;
	}

	void CheckGroundStatus()
	{
		RaycastHit hitInfo;
		#if UNITY_EDITOR
		// helper to visualise the ground check ray in the scene view
		Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * deltaGround));
		#endif
		// 0.1f is a small offset to start the ray from inside the character
		// it is also good to note that the transform position in the sample assets is at the base of the character
		if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, deltaGround))
		{
			surfaceNormal = hitInfo.normal;
			isGrounded = true;
			animator.applyRootMotion = true;
		}
		else
		{
			isGrounded = false;
			surfaceNormal = Vector3.up;
			animator.applyRootMotion = false;
		}

	}
	void HandleAirborneMovement()
	{
		// apply extra gravity from multiplier:
		Vector3 extraGravityForce = (Physics.gravity * 2) - Physics.gravity;
		rigidbody.AddForce(extraGravityForce);

//		m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
	}


    public void Death()
    {
        animator.Play("death");
        Invoke("DeadScreen", 2);
    }

    public void DeadScreen()
    {
        AnimalSim.gameController.Lose();
    }

    public void NewLife()
    {
        animator.Play("IdleRunWalk");
        AnimalSim.playerLiving.FullHP();
    }
}
