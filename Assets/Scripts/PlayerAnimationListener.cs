﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationListener : MonoBehaviour {

	private AnimalController animal;

	// Use this for initialization
	void Awake () {		
		animal = GetComponent<AnimalController> ();
	}

	public void HitTarget(){
		animal.Head.SendMessage ("HitTarget");
	}
}
