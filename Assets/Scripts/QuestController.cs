﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using System;

[System.Serializable]
public class QuestUI
{
	public Text TitleTxt;
	public Transform QuestsContent;
	public Text DescTxt;
    public Text PointsTxt;

	[Space(5)]
	public GameObject QuestItemPrefab;
	public Slider questProgress;
	public Text questTxt;
}

public class QuestController : MonoBehaviour {

	private QuestList questList;
	private int curQuestIndex = 1;
	private Quest curQuest;
	public QuestUI questUI;

	private int currentKills=0;
    private int points = 0;

	private string rootFolder
	{
		get 
		{
			string folder="";
			#if UNITY_EDITOR
			folder = Application.dataPath;
			#elif UNITY_ANDROID || UNITY_IOS
			folder = Application.persistentDataPath;
			#endif
			return folder;
		}
	}

	private void SelectQuest(int index)
	{
		Quest quest = questList.Quests.Where (x => x.Id == index).FirstOrDefault ();			
		curQuest = quest;
        

        Debug.Log ("Select quest: " + quest.Id);
		FillUI ();

		foreach (QuestItem questItem in curQuest.Targets) 
		{
			AnimalSim.animalPopulation.NeedSpawnAnimal (questItem.Name, questItem.TotalCount);
		}

	}

	private void RandomQuest()
	{
		Quest quest = new Quest ();
		quest.RandomQuest ();
		curQuest = quest;
		Debug.Log ("Select quest: " + quest.Id);
		FillUI ();

		foreach (QuestItem questItem in curQuest.Targets) 
		{
			AnimalSim.animalPopulation.NeedSpawnAnimal (questItem.Name, questItem.TotalCount);
		}
	}

	public void FindNearestTarget()
	{
		
	}

	private string QuestsXml
	{
		get 
		{ 
			return  rootFolder + "/" + "XML" + "/QuestsLibrary.xml";
		}
	}

	void Awake()
	{
		//Init ();
        questUI.PointsTxt.text = points.ToString();
        RandomQuest ();
        //SelectQuest (curQuestIndex);
    }

	void ClearUI()
	{
		foreach (Transform child in questUI.QuestsContent)
		{
			GameObject.Destroy (child.gameObject);
		}
	}

	void FillUI()
	{
		ClearUI ();

		questUI.TitleTxt.text = "Quest " + curQuestIndex;
		questUI.DescTxt.text=curQuest.RewardPoints+" points"+Environment.NewLine+curQuest.RewardExperience+" exp";

		foreach (QuestItem questItem in curQuest.Targets)
		{
			GameObject obj = (GameObject)Instantiate (questUI.QuestItemPrefab, Vector3.zero, Quaternion.identity);
			obj.transform.SetParent(questUI.QuestsContent, false);
			QuestItemUI itemUI = obj.GetComponent<QuestItemUI> ();
			itemUI.SetItem (questItem);
		}

		ShowQuestProgress ();
	}

	public void AnimalWasKilled(string AnimalName)
	{
		QuestItem item = curQuest.Targets.Where (x => x.Name == AnimalName).FirstOrDefault ();

        if (item!=null)
		item.CurrentCount++;

		Debug.Log ("Animal killed: " + AnimalName);        
		currentKills++;
		FillUI ();

        //Check quest finish
        if (curQuest.isFinished())
        {
            points += curQuest.RewardPoints;
            questUI.PointsTxt.text = points.ToString();
            AnimalSim.levelManager.LevelUp();
            //new quest
            RandomQuest();
        }

    }

	public void ShowQuestProgress()
	{
        Debug.Log("QUEST KILLS: " + curQuest.KilledAnimals());
        Debug.Log("QUEST TOTAL ANIMALS: " + curQuest.TotalAnimals);
        float progress = (float)curQuest.KilledAnimals() / curQuest.TotalAnimals;
        Debug.Log("QUEST PROGRESS: " + progress);
		questUI.questTxt.text = (int)(progress * 100) + " %";
		questUI.questProgress.value = (progress * 100);

	}

	#region Helper

	private void Init()
	{
		DirectoryInfo dirInf = new DirectoryInfo(rootFolder + "/" + "XML");
		if (!dirInf.Exists) {  //Check if directory exists
			Debug.Log ("Creating directory!");
			try {
				dirInf.Create ();
			} catch (UnityException e) {
				Debug.Log (e);
			}
		} else {
			Debug.Log ("Directory already exist!");
		}

		Load ();
	}
	public void Save()
	{		
		questList.Save ();
	}

	public void Load()
	{
		if (File.Exists (QuestsXml)) {

			Debug.Log ("Activities new_Database already exist doing nothing!");

			//Check New Version Shop XML
			questList = QuestList.Load ();

			TextAsset shopXML = Resources.Load ("QuestsLibraryDefault") as TextAsset;
			QuestList newShopList = QuestList.LoadFromText (shopXML.text);

			if (questList.Version < newShopList.Version) {
				Debug.Log ("Detect lower version SHOP XML");
				questList = newShopList;
				questList.Save ();
			}

		}
		else
		{
			Debug.Log("Activities Database does not exist!");
			try
			{  
				TextAsset shopXML = Resources.Load ("QuestsLibraryDefault") as TextAsset;
				if (shopXML!=null){
					questList = QuestList.LoadFromText (shopXML.text);
					questList.Save ();
				}
				else {
					Debug.Log("Can't find StoreDefault");
				}

			}
			catch(UnityException e)
			{

			}
		}


	}

	#endregion
}
