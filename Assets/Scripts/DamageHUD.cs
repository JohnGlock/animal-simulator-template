﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DamageHUD : MonoBehaviour
{
	public Vector3 OffSet = Vector3.zero;
	public float Multipler = 0.002f;
	private Text text;
	private Transform player;
	private Transform m_Transform = null;


	// Use this for initialization
	void Awake ()
	{
		text = GetComponent<Text> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;	

	}

	public void SetUI(int damage)
	{
		text.text = "- " + damage;	
		Destroy (gameObject, 2);
	}

	void Update()
	{
		Camera c = Camera.main;
		//Calculate the size and position of ui
		float distance = Vector3.Distance(player.position, c.transform.position);
		float d = (distance * 0.0025f);

//		if (d >= 1.20f)
//		{
//			d = 1.20f;
//		}
//		if (d <= 0.15f)
//		{
//			d = 0.15f;
//		}

		//Follow Ui to the target in position and rotation
		transform.localScale = new Vector3(d, d, d);
		transform.rotation = c.transform.rotation;
		Vector3 position = transform.position;
		position.y += 2f * Time.deltaTime;
		transform.position = position;	

	}
}
