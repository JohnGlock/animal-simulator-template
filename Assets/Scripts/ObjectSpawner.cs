﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject[] ObjectsToSpawn;
    private List<GameObject> spawned = new List<GameObject>();
    private GameObject player;
    public int Radius = 150, Max = 1, ActivateDistance=80;
    // Use this for initialization
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        PrepareForSpawn();
        InvokeRepeating("PrepareForSpawn", 0, 20);
    }	


    private void OnDestroy()
    {
        StopAllCoroutines();    
    }

    void PrepareForSpawn()
    {
        spawned.RemoveAll(t => t == null);
        if (spawned.Count < Max)
        {
            Spawn();
        }
    }

    bool CalcPosition(out Vector3 position)
    {
        Vector3 spawnpos = Vector3.ClampMagnitude(new Vector3(UnityEngine.Random.Range(-Radius, Radius), 0, UnityEngine.Random.Range(-Radius, Radius)), Radius);
        spawnpos += transform.position;

        NavMeshHit nh;
        NavMesh.SamplePosition(spawnpos, out nh, 20, 1);
        position = nh.position;

        return true;
    }

    void Spawn()
    {
        Vector3 spawnpos;
        if (!CalcPosition(out spawnpos)) { return; }
        int r = UnityEngine.Random.Range(0, ObjectsToSpawn.Length);
        GameObject tmp = (GameObject)Instantiate(ObjectsToSpawn[r].gameObject, spawnpos, Quaternion.identity);
        spawned.Add(tmp);
    }

	public void ForceSpawn(GameObject animalPrefab)
	{
		Vector3 spawnpos;
		if (!CalcPosition(out spawnpos)) { return; }
		GameObject tmp = (GameObject)Instantiate(animalPrefab, spawnpos, Quaternion.identity, transform);
		spawned.Add(tmp);
		Debug.Log ("Force Spawn " + animalPrefab.name);
	}

    public float PlayerDist()
    {
        Vector3 tpos = transform.position;
        tpos.y = player.transform.position.y;
        float res = Vector3.Magnitude(player.transform.position - tpos) - Radius;
        return res > 0 ? res : 0;
    }
    
#if UNITY_EDITOR

    public void OnDrawGizmos() {
        
        UnityEditor.Handles.color = Color.yellow;
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.up, Radius);

        Gizmos.DrawIcon(transform.position,"");
    }

#endif

}
