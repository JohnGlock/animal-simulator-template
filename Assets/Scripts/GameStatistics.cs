﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("GameStatistics")]
public class GameStatistics : XMLCfg {

	private static GameStatistics instance;

	[XmlIgnore]
	new public string FileName = "GameStatistics";

	public int totalDeaths=0;
	public int levelNumber=1;

	public static GameStatistics Instance()
	{
		if (instance == null) {
            instance =(GameStatistics)LoadXML(rootFolder + "/" + "XML" + "/GameStatistics.xml");
            if (instance == null)
            {
                instance = new GameStatistics();
                instance.Save();
            }
		}
		return instance;
	}

	public GameStatistics()
	{
        Debug.Log("Game Stats Init");
		base.FileName = this.FileName;

	}
}
