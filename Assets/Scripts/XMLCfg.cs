﻿using UnityEngine;
using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

[XmlInclude(typeof(GameStatistics))]
public class XMLCfg{
	
	[XmlIgnore]
	public string FileName="XML";

	[XmlIgnore]
	public static string rootFolder
	{
		get 
		{
			string folder="";
			#if UNITY_EDITOR
			folder = Application.dataPath;
			#elif UNITY_ANDROID || UNITY_IOS
			folder = Application.persistentDataPath;
			#endif
			return folder;
		}
	}

	public string XMLFile()
	{
		return  rootFolder + "/" + "XML" + "/"+FileName+".xml";
	}


	#region Helper

	public void Init()
	{
		DirectoryInfo dirInf = new DirectoryInfo(rootFolder + "/" + "XML");
		if (!dirInf.Exists) {  //Check if directory exists
			Debug.Log ("Creating directory!");
			try {
				dirInf.Create ();
			} catch (UnityException e) {
				Debug.Log (e);
			}
		} else {
			Debug.Log ("Directory already exist!");
		}

		Load ();
	}

	public void Load()
	{
		if (File.Exists (XMLFile())) {

			Debug.Log ("Activities new_Database already exist doing nothing! Load data");

		}
		else
		{
			Debug.Log("Activities Database does not exist! Create Empty");
			try
			{  
				Save();

			}
			catch(UnityException e)
			{

			}
		}


	}
	#endregion

	#region Load / Save & Serialization

	public void Save()
	{
		Save (XMLFile());
	}

	public void Save(string path)
	{
		var serializer = new XmlSerializer(typeof(XMLCfg));
		using(var stream = new FileStream(path, FileMode.Create))
		{
			serializer.Serialize(stream, this);
		}
	}
    
	public static XMLCfg LoadXML (string path="")
	{
        if (File.Exists(path))
        {
            var serializer = new XmlSerializer(typeof(XMLCfg));
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as XMLCfg;
            }
        }
        else
        {
            return null;
        }
	}

	//Loads the xml directly from the given string. Useful in combination with www.text.
	public QuestList LoadFromText(string text) 
	{
		var serializer = new XmlSerializer(typeof(QuestList));
		return serializer.Deserialize(new StringReader(text)) as QuestList;
	}


	#endregion

}
