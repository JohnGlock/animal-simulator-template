﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class Raycaster : MonoBehaviour {

	public GameObject bloodEffect;
	public GameObject damageHUD;
	public Canvas gameCanvas;
	public Slider EnemyHPBar;
	public Text EnemyHPText;
	public Image EnemyHPAvatar;
	public Slider EatingBar;
	
	private AnimalController animalController;
	private Animator anim;
	public bool isEating;
	public GameObject curTarget;
	public Animal animal;


	void Awake()
	{
		animalController = GetComponentInParent<AnimalController> ();
		anim = GetComponentInParent<Animator> ();
		EatingBar.gameObject.SetActive (false);
		EnemyHPBar.gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider col) 
	{
		if (col.gameObject.tag == "Respawn" || col.gameObject.tag == "DeadBody") {
			curTarget= col.gameObject.transform.root.gameObject;
		}
	}

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Respawn" || col.gameObject.tag == "DeadBody")
        {
            curTarget = col.gameObject.transform.root.gameObject;
        }
    }

    void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Respawn" || col.gameObject.tag == "DeadBody") {
            curTarget = null;

        }
	}

	public void DeleteKilledEnemy(GameObject go)
	{
        curTarget = null;
    }

	void Update()
	{
		if (curTarget) {
			animal = curTarget.GetComponent<Animal> ();
		} else {
			isEating = false;
			anim.SetBool ("Eating", false);
			animal = null;
		}
		
		if (CrossPlatformInputManager.GetButtonDown ("Fire1")) {
			Attack ();
		}

		InteractiveHPBar ();
		Eating ();
	}


	void Eating()
	{
		EatingBar.gameObject.SetActive (isEating);

		if (isEating) {			
			float totalEatingTime = animal.eatingTime;
			float curEatingTime = animal.curEatingTime;
			EatingBar.maxValue = totalEatingTime;
			EatingBar.value = curEatingTime;

			int healValue = (int)(animal.maxHeal / animal.eatingTime);
			AnimalSim.playerLiving.GetHeal (Time.deltaTime*healValue);

			animal.curEatingTime -= Time.deltaTime;
			if (animal.curEatingTime <= 0) {
				isEating = false;
				Destroy (animal.gameObject);
			}
		}
	}

	void InteractiveHPBar()
	{              
		if (animal == null) {
			EnemyHPBar.gameObject.SetActive (false);
			return;
		}

		Animal animalAI = animal;
		if (!animalAI.IsDead) {
			EnemyHPText.text = animalAI.Health + "/" + animalAI.maxHealth;
			EnemyHPBar.maxValue = animalAI.maxHealth;
			EnemyHPBar.value = animalAI.Health;
			EnemyHPBar.gameObject.SetActive (true);
			EnemyHPAvatar.overrideSprite = SpriteLibrary.Instance ().GetSpriteByName (animalAI.Name.ToLower () + "_green");
		} 
		else 
		{	
			EnemyHPBar.gameObject.SetActive (false);
		}

	}

	void Attack()
	{
		if (animal != null) {
			if (animal.IsDead) {
				//Eating
				isEating=true;
				anim.SetBool ("Eating", true);
			} else {
				isEating=false;
				anim.SetBool ("Eating", false);
				anim.SetTrigger ("Attack");
			}
		} else {
			anim.SetTrigger ("Attack");
			anim.SetBool ("Eating", false);
		}
	}

	void HitTarget()
	{
		Debug.Log ("USER ATTACK!");
		RaycastHit hit;

		if (animal) {
			Vector3 hitPoint = transform.position;
			Instantiate(bloodEffect, hitPoint, Quaternion.identity);
			GameObject damageGO=Instantiate(damageHUD);
			DamageHUD damageUI = damageGO.GetComponent<DamageHUD> ();
			damageUI.transform.SetParent (gameCanvas.transform);
			damageUI.transform.position = hitPoint;

			int minDmg = 6+(animalController.damageBoost*3);
			int maxDmg = minDmg + ((animalController.damageBoost+1) * 3);
			int randomDamage = Random.Range (minDmg, maxDmg);
			damageUI.SetUI (randomDamage);
			animal.SendMessage("GetHit", randomDamage);
			UISounds.PlaySound("attack");
		}
		else
		{
			UISounds.PlaySound("attack");
		}

	}
}
