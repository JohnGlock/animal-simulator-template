using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityStandardAssets.CrossPlatformInput {
    [RequireComponent(typeof(Image))]
    public class TouchPadX : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

        /*
		#pragma strict
		
		private var h : float;
		private var v : float;
		private var horozontalSpeed : float = 2.0;
		private var verticalSpeed : float = 2.0;
		
		function Update()
		{
			if (Input.touchCount == 1)
			{
				var touch : Touch = Input.GetTouch(0);
				
				if (touch.phase == TouchPhase.Moved)
				{
					h = horozontalSpeed * touch.deltaPosition.x ;
					transform.Rotate( 0, -h, 0, Space.World );
					
					v = verticalSpeed * touch.deltaPosition.y ;
					transform.Rotate( v, 0, 0, Space.World );
				}
			}
		}
		*/

        // Options for which axes to use
        public enum AxisOption {
            Both,
            // Use both
            OnlyHorizontal,
            // Only horizontal
            OnlyVertical
            // Only vertical
        }


        public enum ControlStyle {
            Absolute,
            // operates from teh center of the image
            Relative,
            // operates from the center of the initial touch
            Swipe,
            // swipe to touch touch no maintained center
        }


        public AxisOption axesToUse = AxisOption.Both;
        // The options for the axes that the still will use
        public ControlStyle controlStyle = ControlStyle.Absolute;
        // control style to use
        public string horizontalAxisName = "Horizontal";
        // The name given to the horizontal axis for the cross platform input
        public string verticalAxisName = "Vertical";
        // The name given to the vertical axis for the cross platform input
        public float Xsensitivity = 1f;
        public float Ysensitivity = 1f;

        public enum GestureType { None, Pinch};

        public GestureType UseGesture;
        public string GestureAxisName = "";

        public bool isFIRE;

        Vector3 m_StartPos;
        Vector2 m_PreviousDelta;
        Vector3 m_JoytickOutput;
        bool m_UseX;
        // Toggle for using the x axis
        bool m_UseY;
        // Toggle for using the Y axis
        CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis;
        // Reference to the joystick in the cross platform input
        CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis;
        // Reference to the joystick in the cross platform input
        CrossPlatformInputManager.VirtualAxis m_GestureVirtualAxis;

        bool m_Dragging;
        int m_Id = -1;
        int m1_Id = -1;
        Vector2 m_PreviousTouchPos, m_PreviousTouchPos1;
        // swipe style control touch

        float sense = 1f;

        public void SetSwipeSense(float newSense) {
            sense = newSense;
        }

#if !UNITY_EDITOR
    private Vector3 m_Center;
    private Image m_Image;

#endif
        Vector3 m_PreviousMouse;
//#endif

        void OnEnable() {
            if (!isFIRE) {
                CreateVirtualAxes();
            } else {
                // set axes to use


                // create new axes based on axes to use
                if (m_UseX) {
                    //m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
                    //CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);

                }
                if (m_UseY) {
                    //m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
                    //CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
                }
            }
        }

        void Start() {
#if !UNITY_EDITOR
            m_Image = GetComponent<Image>();
            m_Center = m_Image.transform.position;
#endif

            if (isFIRE) {
                m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
                m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

                m_HorizontalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(horizontalAxisName);
                m_VerticalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(verticalAxisName);

                if (UseGesture != GestureType.None)
                    m_GestureVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(GestureAxisName);
            }
        }

        void CreateVirtualAxes() {
            // set axes to use
            m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
            m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

            // create new axes based on axes to use
            if (m_UseX && m_HorizontalVirtualAxis==null) {
                m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
            }
            if (m_UseY && m_VerticalVirtualAxis==null) {
                m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
            }

            if (UseGesture != GestureType.None && m_GestureVirtualAxis==null) {
                m_GestureVirtualAxis = new CrossPlatformInputManager.VirtualAxis(GestureAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_GestureVirtualAxis);
            }

        }

        float maxxx = 0;
        [SerializeField] float zoom = 1;

        public void SetSense(float Sence) {
            zoom = Sence;
        }

        void UpdateGestureAxes(Vector2 p0, Vector2 p1) {
            var v0 = m_PreviousTouchPos1 - m_PreviousTouchPos;
            var v1 = p1 - p0;

            float ax = v1.magnitude - v0.magnitude;

            if(UseGesture!=GestureType.None)
                m_GestureVirtualAxis.Update(ax);
            //print(ax);
        }

        void UpdateVirtualAxes(Vector3 value) {
            
            value *= 6.0f;
            
            if (value.magnitude > 2.5f) {
                value = value.normalized * 2.5f;
            }
            
            //
            
            if (value.magnitude < 0.002f) {
                value = new Vector3(0, 0, 0);
            }

            value *= 17f;
                        
            if (m_UseX) {
                m_HorizontalVirtualAxis.Update(value.x * 1.35f * zoom * sense);
            }

            if (m_UseY) {
                m_VerticalVirtualAxis.Update(value.y * zoom * sense);
            }
        }


        public void OnPointerDown(PointerEventData data) {


            m_Dragging = true;

            if (UseGesture != GestureType.None) {
                if (m_Id > -1) {
                    m1_Id = data.pointerId;
                } else {
                    m_Id = data.pointerId;
                }
            } else {
                m_Id = data.pointerId;
            }

//#if !UNITY_EDITOR

            print(m_Id);
			//m_Center = Input.touches[m_Id].position;
            if(m_Id>-1)
			    m_PreviousTouchPos = Input.touches[m_Id].position;

            m_PreviousMouse.x = Input.mousePosition.x;
            m_PreviousMouse.y = Input.mousePosition.y;

            if (m1_Id > -1) {
                m_PreviousTouchPos1 = Input.touches[m1_Id].position;
            }
//#else
//            m_PreviousMouse.x = Input.mousePosition.x;
//            m_PreviousMouse.y = Input.mousePosition.y;
//#endif
        }

        void Update() {
            if (m_Id>-1 && m1_Id > -1) { //gesture
                
                var p0 = Input.touches[m_Id].position;
                var p1 = Input.touches[m1_Id].position;

                UpdateGestureAxes(p0, p1);

                m_PreviousTouchPos = Input.touches[m_Id].position;
                m_PreviousTouchPos1 = Input.touches[m1_Id].position;

                return;
            }

            if (!m_Dragging) {
#if !UNITY_EDITOR
				print(zoom);
				if (Input.touchCount >= m_Id + 1 && m_Id != -1)
				{
					m_PreviousTouchPos = Input.touches[m_Id].position;
					m_Center = m_PreviousTouchPos;
				}
#endif
                return;
            }


            if (Input.touchCount >= m_Id + 1 && m_Id != -1) {
#if !UNITY_EDITOR

            if (controlStyle == ControlStyle.Swipe)
            {
                m_Center = m_PreviousTouchPos;
                m_PreviousTouchPos = Input.touches[m_Id].position;
            }
            Vector2 pointerDelta = new Vector2(Input.touches[m_Id].position.x - m_Center.x , Input.touches[m_Id].position.y - m_Center.y);
				pointerDelta.x = pointerDelta.x / Screen.width;
				pointerDelta.y = pointerDelta.y / Screen.height;
#else
                Vector2 pointerDelta;
                pointerDelta.x = (Input.mousePosition.x - m_PreviousMouse.x) / Screen.width;
                pointerDelta.y = (Input.mousePosition.y - m_PreviousMouse.y) / Screen.height;

                m_PreviousMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);

#endif
                UpdateVirtualAxes(new Vector3(pointerDelta.x, pointerDelta.y, 0));
            }
        }


        public void OnPointerUp(PointerEventData data) {
            m_Dragging = false;
            m_Id = -1;
            m1_Id = -1;
            UpdateVirtualAxes(Vector3.zero);
            UpdateGestureAxes(m_PreviousTouchPos,m_PreviousTouchPos1);
        }

        void OnDisable() {
            if (CrossPlatformInputManager.AxisExists(horizontalAxisName))
                CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);

            if (CrossPlatformInputManager.AxisExists(verticalAxisName))
                CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);

            if (CrossPlatformInputManager.AxisExists(GestureAxisName))
                CrossPlatformInputManager.UnRegisterVirtualAxis(GestureAxisName);
        }
    }
}