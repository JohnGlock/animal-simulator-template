﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLiving : MonoBehaviour,HitableObject {

	private float _stamina = 100.0f;
	private float _hp = 100.0f;

	public Slider healthSlider;
	public Text healthText;

	public Slider staminaSlider;
	public Text staminaText;

	public int MaxHP=100;
	public int MaxStamina=100;

	private float playerSpeed = 0f;
    public bool isDead;
    public float Stamina
	{
		get {return _stamina; }
		set
		{
			_stamina = value;
			staminaSlider.value =(int)value;
			staminaText.text = (int)value + "/" + MaxStamina;
		}
	}

	public float HP
	{
		get { return _hp;}
		set
		{
			_hp = value;
			healthSlider.value =(int)value;
			healthText.text = (int)value + "/" + MaxHP;
		}
	}
	// Use this for initialization

	void Start ()
	{
        isDead = false;
        HP = MaxHP;
		Stamina = MaxStamina;
		needUpdateUI ();
	}

	public void needUpdateUI()
	{
        HP =HP;
        Stamina = Stamina;

        healthSlider.maxValue = MaxHP;
		healthSlider.value = HP;

		staminaSlider.maxValue = MaxStamina;
		staminaSlider.value = Stamina;
	}

	public void UpdateStamina(float SpeedValue)
	{
		playerSpeed = SpeedValue;

//		int factor = 0;
//		//Walk
//		if (SpeedValue >= 0.1 && SpeedValue <= 0.5) {
//			factor = 1;
//		}
//		//Run
//		else {
//			factor = 2;
//		}
//
//		if (Stamina > 0) {
//			
//			AnimalSim.playerController.canRun = true;
//
//		} else {
//			AnimalSim.playerController.canRun = false;
//		}
	}


	public float GetStamina()
	{
		return Stamina;
	}

	// Update is called once per frame 
	void Update () 
	{		

		if (playerSpeed >= 0.1) {
			Stamina -= Time.deltaTime*5;
			if (Stamina < 0) {
				Stamina = 0;

			}
		} else {
			if (Stamina < MaxStamina) {
				Stamina += Time.deltaTime*20;
			}
		}
		AnimalSim.playerController.canRun = Stamina != 0;
			
	}

	public void GetHit(float damage)
	{
		HP -= (int)damage;
		if (HP <= 0) {
			HP = 0;
            isDead = true;
            AnimalSim.playerController.Death();
            //DEAD
        }

	}
    
	public void GetHeal(float value)
	{        
		if (HP != 0 && HP<MaxHP) {
			HP += value;
		}
	}

    public void FullHP()
    {
        bool finished_ads = AnimalSim.showAd.PlayUnityVideoAd();
        if (finished_ads)
        {
            HP = MaxHP; 
        }
    }
}
