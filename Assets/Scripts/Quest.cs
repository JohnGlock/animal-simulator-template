﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItem
{
	public string Name;
	public int CurrentCount;
	public int TotalCount;
}

public class Quest 
{
	public int Id;
	public List<QuestItem> Targets; 
	public int RewardPoints;
	public int RewardExperience;
	public int TotalAnimals;
   

    public void RandomQuest()
	{
		Targets = new List<QuestItem> ();
        RewardExperience = 1000;
        RewardPoints = 100+(Random.Range(1,3)*50);
		//levelId < 5 2 targets else 3
		if (0 < 3) {
			for (int i = 0; i < 2; i++) {
				string RandomAnimalName="";
				int animalCount = 0;
				//default animal
				if (i == 0) {
					RandomAnimalName = AnimalSim.Animals [Random.Range (0, 4)];
					animalCount = Random.Range (1, 4);
				}
				//heavy animal
				else {
					RandomAnimalName = AnimalSim.Animals [Random.Range (5, 7)];
					animalCount = 1;
				}

                TotalAnimals += animalCount;
				QuestItem questItem = new QuestItem{ Name = RandomAnimalName, TotalCount = animalCount };

				Targets.Add (questItem);
			}
		} else {
			
		}
        Debug.Log("TOTAL ANIMALS IN QUEST:"+ TotalAnimals);
	}
    public int KilledAnimals()
    {
        int diedAnimals = 0;
        foreach (QuestItem item in Targets)
        {
            diedAnimals += item.CurrentCount;
        }
        return diedAnimals;
    }

    public bool isFinished()
    {        
        return TotalAnimals == KilledAnimals();
    }
}
