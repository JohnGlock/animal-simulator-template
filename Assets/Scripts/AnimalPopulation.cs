﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalPopulation : MonoBehaviour {

	public GameObject[] ObjectsToSpawn;
	public GameObject[] SpawnPoints;

	private GameObject player;

	private List<GameObject> spawned = new List<GameObject>();

	// Use this for initialization
	void Start () 
	{
		player = AnimalSim.playerController.gameObject;
	}

	public void NeedSpawnAnimal(string AnimalName, int Count)
	{
		GameObject prefab = FindAnimalPrefab (AnimalName);
		for (int i = 0; i < Count; i++) {
			GameObject randomSpawn = RandomSpawnPoint();
			ObjectSpawner objectSpawner = randomSpawn.GetComponent<ObjectSpawner> ();
			objectSpawner.ForceSpawn (prefab);
		}
	
	}

	private GameObject FindAnimalPrefab(string name)
	{
		GameObject animalPrefab=null;
		foreach (GameObject prefab in ObjectsToSpawn) {
			string prefabName = prefab.name.ToLower ();
			if (prefabName.IndexOf (name.ToLower ())!=-1) {
				animalPrefab = prefab;
			}
		}
		return animalPrefab;
	}

    private GameObject NearestSpawnPoint()
    {
        float minDistToPlayer = 999999;
        GameObject nearestPlace=new GameObject();
        foreach (GameObject point in SpawnPoints)
        {
            ObjectSpawner objectSpawner = point.GetComponent<ObjectSpawner>();
            if (objectSpawner.PlayerDist() < minDistToPlayer)
            {
                minDistToPlayer = objectSpawner.PlayerDist();
                nearestPlace = point;
            }
        }
        return nearestPlace;
    }

    private GameObject RandomSpawnPoint()
	{
		int rnd = Random.Range (0, SpawnPoints.Length);
		return SpawnPoints [rnd];		
	}

	// Update is called once per frame
	void Update () {
		
	}
}
